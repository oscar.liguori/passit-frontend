import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy
} from "@angular/core";

import { ISecret } from "../../../passit_sdk/api.interfaces";

@Component({
  selector: "app-popup",
  templateUrl: "./popup.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./popup.component.scss"]
})
export class PopupComponent {
  @Input() secrets: ISecret[];
  @Input() selectedSecret: number | null;
  @Input() totalSecretsCount: number;
  @Input() formFillMessage: string;
  @Input() search: string;
  @Input() matchedSecrets: ISecret[];
  @Input() usernameCopied: number | null;
  @Input() passwordCopied: number | null;
  @Input() popupFirstTimeLoadingComplete: boolean;
  @Input() hasGroupInvites: boolean;

  @Output() setSelected = new EventEmitter<number>();
  @Output() closeSelected = new EventEmitter();
  @Output() searchUpdate = new EventEmitter<string>();
  @Output() openFull = new EventEmitter();
  @Output() openUrl = new EventEmitter<ISecret>();
  @Output() signIn = new EventEmitter<ISecret>();
  @Output() onCopyUsername = new EventEmitter<ISecret>();
  @Output() onCopyPassword = new EventEmitter<ISecret>();
  @Output() openFullApp = new EventEmitter();
}
