import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { AbstractControlState, ValidationErrors } from "ngrx-forms";
import { IS_EXTENSION } from "../../constants";

export const ALL_PASSIT_PERMISSIONS = {
  origins: ["<all_urls>"],
};

@Component({
  selector: "app-server-select",
  templateUrl: "./server-select.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServerSelectComponent {
  prompted = false;
  @Input() showUrlControl: AbstractControlState<boolean>;
  @Input() urlControl: AbstractControlState<string>;
  @Input() formErrors: ValidationErrors;
  /** Needed for NS on submit, not used in web */
  @Output() onSubmit = new EventEmitter();

  constructor() {}

  promptForPermission() {
    if (IS_EXTENSION && !this.prompted) {
      this.prompted = true;
      browser.permissions.request(ALL_PASSIT_PERMISSIONS)
    }
  }
}

