import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-heading",
  template: `
    <h2 class="heading-main heading-main--pad-bottom">{{ text }}</h2>
  `,
  styleUrls: ["./heading.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadingComponent {
  @Input() text: string;
}
