import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { CheckboxComponent } from "./checkbox.component";
import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule } from "@ngrx/store";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("CheckboxComponent", () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CheckboxComponent],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({}),
        InlineSVGModule.forRoot(),
        NgrxFormsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    component.title = "Hello there";
    (component.control as any) = {};
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
