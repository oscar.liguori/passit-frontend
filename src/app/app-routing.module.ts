import { ModuleWithProviders, NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StoreRouterConnectingModule } from "@ngrx/router-store";

import { IS_EXTENSION } from "./constants";
import { PopupContainer } from "./extension/popup/popup.container";
import { NoContentContainer } from "./no-content/no-content.container";
import { PopupLoggedInGuard } from "./guards/popup-logged-in.guard";
import { routes } from "./app.common";
import { LoggedInGuard } from "./guards";

const appRoutes: Routes = [
  ...routes,
  {
    path: "popup",
    component: PopupContainer,
    canActivate: [PopupLoggedInGuard],
    data: {
      showNavBar: false,
    },
  },
  {
    path: "organizations",
    canActivate: [LoggedInGuard],
    loadChildren: () =>
      import("./organizations/organizations.module").then(
        (mod) => mod.OrganizationsModule
      ),
  },
  {
    path: "**",
    component: NoContentContainer,
  },
];

const routing: ModuleWithProviders<AppRoutingModule> = RouterModule.forRoot(
  appRoutes,
  // use hashing when it is an extension, because the browsers can't
  // redirect other urls for SPA to the index.html
  {
    useHash: IS_EXTENSION,
  }
);
export const routingStore = StoreRouterConnectingModule;

@NgModule({
  imports: [routing],
  exports: [RouterModule],
})
export class AppRoutingModule {}
