import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import {
  VerifyU2fActionTypes,
  VerifyU2fSuccess,
  VerifyU2fFailure,
} from "./verify-u2f.actions";
import {
  withLatestFrom,
  map,
  exhaustMap,
  catchError,
  tap,
  filter
} from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import { UserService } from "../../../account/user";
import { of } from "rxjs";
import { Router } from "@angular/router";
import { IS_EXTENSION } from "../../../constants";
import { VerifyMfaActionTypes } from "../verify-mfa.actions";
import { IState, getSkipU2F } from "../../login.reducer";

@Injectable()
export class VerifyU2FEffects {
  @Effect()
  verifyU2f$ = this.actions$.pipe(
    ofType(VerifyU2fActionTypes.VERIFY_U2F, VerifyMfaActionTypes.SWITCH_MFA_METHOD),
    withLatestFrom(this.store.pipe(select(getSkipU2F))),
    filter(([action, skipU2F]) => !skipU2F),
    exhaustMap(() => {
        return this.service.verifyU2f().pipe(
            map(resp => new VerifyU2fSuccess()),
            catchError(resp => of(new VerifyU2fFailure()))
        );
        })
    );

  @Effect({ dispatch: false })
  verifyU2fSuccess$ = this.actions$.pipe(
    ofType(VerifyU2fActionTypes.VERIFY_U2F_SUCCESS),
    tap(() => {
      if (IS_EXTENSION) {
        this.router.navigate(["/popup"]);
      } else {
        this.router.navigate(["/"]);
      }
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private service: UserService,
    private router: Router
  ) {}
}
