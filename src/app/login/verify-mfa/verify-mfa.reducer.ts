import {
  updateGroup,
  validate,
  createFormGroupState,
  FormGroupState,
  createFormStateReducerWithUpdate,
  markAsSubmitted
} from "ngrx-forms";
import { minLength, maxLength, required } from "ngrx-forms/validation";
import { IBaseFormState } from "../../utils/interfaces";
import { VerifyMfaActionTypes, VerifyMfaActions } from "./verify-mfa.actions";
import {
  ResetFormActionTypes,
  ResetFormActionsUnion
} from "../../form/reset-form.actions";

const FORM_ID = "Login Form";

export interface IVerifyMfaForm {
  otp: string;
}

const validateAndUpdateFormState = updateGroup<IVerifyMfaForm>({
  otp: validate(required, minLength(6), maxLength(6))
});

const initialFormState = validateAndUpdateFormState(
  createFormGroupState<IVerifyMfaForm>(FORM_ID, {
    otp: ""
  })
);

export interface IVerifyMfaState extends IBaseFormState {
  form: FormGroupState<IVerifyMfaForm>;
  errorMessage: string | null;
  skipU2F: boolean;
}

export const initialState: IVerifyMfaState = {
  form: initialFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null,
  skipU2F: false
};

export const formReducer = createFormStateReducerWithUpdate<IVerifyMfaForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: VerifyMfaActions | ResetFormActionsUnion
): IVerifyMfaState {
  const form = formReducer(state.form, action);
  state = { ...state, form };
  switch (action.type) {
    case ResetFormActionTypes.RESET_FORMS:
      return { ...initialState };

    case VerifyMfaActionTypes.VERIFY_MFA:
      return {
        ...state,
        form: markAsSubmitted(state.form),
        hasStarted: true,
        hasFinished: false
      };

    case VerifyMfaActionTypes.VERIFY_MFA_SUCCESS:
      return {
        ...state,
        hasFinished: true
      };

    case VerifyMfaActionTypes.VERIFY_MFA_FAILURE:
      return {
        ...state,
        errorMessage: "Unable to verify code.",
        hasFinished: false,
        hasStarted: false
      };

    case VerifyMfaActionTypes.SWITCH_MFA_METHOD:
      return {
          ...state,
          skipU2F: !state.skipU2F
        };

  }

  return state;
}
