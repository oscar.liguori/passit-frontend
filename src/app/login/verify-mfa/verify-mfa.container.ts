import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Router } from "@angular/router";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import {
  IState,
  selectVerifyMfaForm,
  getVerifyMfaHasStarted,
  getVerifyMfaFinished,
  getVerifyMfaErrorMessage,
  getSkipU2F,
  getU2fErrorMessage,
} from "../login.reducer";
import { VerifyMfa, SwitchMfaMethod } from "./verify-mfa.actions";
import * as fromRoot from "../../app.reducers";
import { IS_EXTENSION } from "../../constants";
import { LogoutAction } from "../../account/account.actions";
import { ResetFormDirective } from "../../form";
import { VerifyU2f } from "./verify-u2f/verify-u2f.actions";

@Component({
  template: `
    <app-verify-mfa
      [form]="form$ | async"
      [form]="form$ | async"
      [isExtension]="isExtension"
      [hasStarted]="hasStarted$ | async"
      [hasFinished]="hasFinished$ | async"
      [errorMessage]="errorMessage$ | async"
      [isPopup]="isPopup"
      [hasU2F]="hasU2F$ | async"
      [useU2F]="useU2F$ | async"
      (goToLogin)="goToLogin()"
      (onSubmit)="onSubmit()"
      (switchMfaMethod)="switchMfaMethod()"
    ></app-verify-mfa>
    <app-verify-u2f
      [isExtension]="isExtension"
      [useU2F]="useU2F$ | async"
      [u2fErrorMessage]="u2fErrorMessage$ | async"
      (goToLogin)="goToLogin()"
      (switchMfaMethod)="switchMfaMethod()"
      (verifyU2f)="verifyU2f()"
    ></app-verify-u2f>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerifyMfaContainer extends ResetFormDirective {
  form$ = this.store.pipe(select(selectVerifyMfaForm));
  hasStarted$ = this.store.pipe(select(getVerifyMfaHasStarted));
  hasFinished$ = this.store.pipe(select(getVerifyMfaFinished));
  errorMessage$ = this.store.pipe(select(getVerifyMfaErrorMessage));
  hasU2F$ = this.store.pipe(select(fromRoot.hasU2F));
  u2fErrorMessage$ = this.store.pipe(select(getU2fErrorMessage));

  useU2F$ = combineLatest([
    this.hasU2F$,
    this.store.pipe(select(getSkipU2F)),
  ]).pipe(map(([hasU2f, skipU2F]) => hasU2f && !skipU2F));

  isExtension = IS_EXTENSION;
  isPopup = false;

  constructor(private router: Router, public store: Store<IState>) {
    super(store);
    store
      .pipe(select(fromRoot.getIsPopup))
      .subscribe((isPopup) => (this.isPopup = isPopup));
  }

  goToLogin() {
    // Technically, the user is logged in during this stage. Log them out before redirecting.
    this.store.dispatch(new LogoutAction());
    if (this.isPopup) {
      browser.tabs.create({
        url: "/index.html#/account/login",
      });
      window.close();
    } else {
      this.router.navigate(["/account/login"]);
    }
  }

  onSubmit() {
    this.store.dispatch(new VerifyMfa());
  }

  switchMfaMethod() {
    this.store.dispatch(new SwitchMfaMethod());
  }

  verifyU2f() {
    this.store.dispatch(new VerifyU2f());
  }
}
