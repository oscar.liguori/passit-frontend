import { TestBed } from "@angular/core/testing";
import { GeneratorService } from "./generator.service";

describe("Generator Service", () => {
  let service: GeneratorService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [GeneratorService]
    });
    service = TestBed.inject(GeneratorService);
  });

  it("generates a random password with default params", async () => {
    const password = await service.generatePassword();
    expect(password.length).toBe(20);
  });

  it("generates a random password of set length", async () => {
    const password = await service.generatePassword({ howMany: 2 });
    expect(password.length).toBe(2);
  });
  it("generates a random password of set allowed chars", async () => {
    const password = await service.generatePassword({ chars: "ab" });
    expect(/^[ab]+$/.test(password)).toBeTruthy();
    expect(/^[c-zA-Z0-9]+$/.test(password)).toBeFalsy();
  });
});
