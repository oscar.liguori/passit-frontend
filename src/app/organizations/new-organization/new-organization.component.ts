import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import { NewOrganizationFormValue } from "../organizations.reducers";

@Component({
  selector: "app-new-organization",
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: "new-organization.component.html",
})
export class NewOrganizationComponent {
  @Input() form: FormGroupState<NewOrganizationFormValue>;
  @Output() onSubmit = new EventEmitter();
  @Output() onCancel = new EventEmitter();
}
