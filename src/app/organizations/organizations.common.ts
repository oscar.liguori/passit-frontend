import { Routes } from "@angular/router";
import { OrganizationsContainer } from "./organizations.container";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: OrganizationsContainer,
  },
];
