export interface NewOrganization {
  name: string;
}

export interface Organization extends NewOrganization {
  id: number;
  slug: string;
}
