import { createReducer, on, Action } from "@ngrx/store";
import {
  createFormGroupState,
  FormGroupState,
  validate,
  updateGroup,
  wrapReducerWithFormStateUpdate,
  onNgrxForms
} from "ngrx-forms";
import { equalTo, required } from "ngrx-forms/validation";
import { passwordValidators } from "../../constants";
import * as SetPasswordActions from "./set-password.actions";
import * as ResetPasswordVerifyActions from "../reset-password-verify/reset-password-verify.actions";

export const FORM_ID = "(Reset) Set Password Form";

export interface ISetPasswordForm {
  newPassword: string;
  newPasswordConfirm: string;
  showConfirm: boolean;
}

export interface ISetPasswordState {
  form: FormGroupState<ISetPasswordForm>;
  hasStarted: boolean;
  backupCode: string | null;
}

export const validateAndUpdateFormState = updateGroup<ISetPasswordForm>({
  newPassword: (newPassword, form) => {
    return validate(newPassword, [...passwordValidators]);
  },
  newPasswordConfirm: (passwordConfirm, form) => {
    if (form.controls.showConfirm.value) {
      return validate(
        passwordConfirm,
        required,
        equalTo(form.value.newPassword)
      );
    }
    return validate(passwordConfirm, []);
  }
});

const initialFormState = validateAndUpdateFormState(
  createFormGroupState<ISetPasswordForm>(FORM_ID, {
    newPassword: "",
    newPasswordConfirm: "",
    showConfirm: true
  })
);

export const initialState: ISetPasswordState = {
  form: initialFormState,
  hasStarted: false,
  backupCode: null
};

export const setPasswordReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(SetPasswordActions.setPassword, state => ({ ...state, hasStarted: true })),
  on(SetPasswordActions.setPasswordFailure, state => ({
    ...state,
    hasStarted: false
  })),
  on(SetPasswordActions.setPasswordSuccess, state => ({
    ...state,
    hasStarted: false,
    backupCode: null
  })),
  on(ResetPasswordVerifyActions.verifyAndLogin, (state, action) => ({
    ...state,
    backupCode: action.payload
  }))
);

const wrappedReducer = wrapReducerWithFormStateUpdate(
  setPasswordReducer,
  s => s.form,
  validateAndUpdateFormState
);
export function reducer(state: ISetPasswordState | undefined, action: Action) {
  return wrappedReducer(state, action);
}

export const getForm = (state: ISetPasswordState) => state.form;
export const getHasStarted = (state: ISetPasswordState) => state.hasStarted;
export const getBackupCode = (state: ISetPasswordState) => state.backupCode;
