import {
  updateGroup,
  validate,
  createFormGroupState,
  FormGroupState,
  createFormStateReducerWithUpdate
} from "ngrx-forms";
import { required, pattern } from "ngrx-forms/validation";

import { IBaseFormState } from "../../utils/interfaces";
import {
  ResetPasswordActionsUnion,
  ResetPasswordActionTypes
} from "./reset-password.actions";
import { environment } from "~/environments/environment";
import { DEFAULT_API } from "~/app/constants";

const FORM_ID = "Reset Password Form";

export interface IResetPasswordForm {
  email: string;
  showUrl: boolean;
  url: string;
}

const validateAndUpdateFormState = updateGroup<IResetPasswordForm>({
  email: validate(required, pattern(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))
});

export const initialFormState = validateAndUpdateFormState(
  createFormGroupState<IResetPasswordForm>(FORM_ID, {
    email: "",
    showUrl: false,
    url: environment.extension ? DEFAULT_API : ""
  })
);

export interface IResetPasswordState extends IBaseFormState {
  form: FormGroupState<IResetPasswordForm>;
  errorMessage: string | null;
  emailDisplayName: string;
}

export const initialState: IResetPasswordState = {
  form: initialFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null,
  emailDisplayName: ""
};

export const formReducer = createFormStateReducerWithUpdate<IResetPasswordForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: ResetPasswordActionsUnion
): IResetPasswordState {
  const form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case ResetPasswordActionTypes.SUBMIT_FORM:
      return {
        ...state,
        hasStarted: true,
        hasFinished: false
      };

    case ResetPasswordActionTypes.SUBMIT_FORM_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        emailDisplayName: form.value.email
      };

    case ResetPasswordActionTypes.SUBMIT_FORM_FAILURE:
      return {
        ...state,
        errorMessage: "An error occured",
        hasFinished: false,
        hasStarted: false
      };

    case ResetPasswordActionTypes.RESET_FORM:
      return initialState;
  }

  return state;
}

export const getForm = (state: IResetPasswordState) => state.form;
export const getErrorMessage = (state: IResetPasswordState) =>
  state.errorMessage;
export const getHasStarted = (state: IResetPasswordState) => state.hasStarted;
export const getHasFinished = (state: IResetPasswordState) => state.hasFinished;
export const getEmailDisplayName = (state: IResetPasswordState) =>
  state.emailDisplayName;
