import { Action } from "@ngrx/store";

export enum AccountActionTypes {
  LOGOUT = "Logout",
  LOGOUT_SUCCESS = "Logout Success",
  SET_URL = "SET_URL",
  HANDLE_API_ERROR = "Handle API Error",
  CRITICAL_API_ERROR = "Critical API Error - should never happen",
  API_FAILURE_NETWORK_DOWN = "API Failure due to network being down",
  USER_MUST_CONFIRM_EMAIL = "User must confirm email"
}

export class LogoutAction implements Action {
  readonly type = AccountActionTypes.LOGOUT;
}

export class LogoutSuccessAction implements Action {
  readonly type = AccountActionTypes.LOGOUT_SUCCESS;
}

export class SetUrlAction implements Action {
  readonly type = AccountActionTypes.SET_URL;

  constructor(public payload: string) {}
}

export class HandleAPIErrorAction implements Action {
  readonly type = AccountActionTypes.HANDLE_API_ERROR;

  constructor(public payload: any) {}
}

export class APIFailureNetworkDownAction implements Action {
  readonly type = AccountActionTypes.API_FAILURE_NETWORK_DOWN;
}

export class UserMustConfirmEmailAction implements Action {
  readonly type = AccountActionTypes.USER_MUST_CONFIRM_EMAIL;
}

export class CriticalAPIErrorAction implements Action {
  readonly type = AccountActionTypes.CRITICAL_API_ERROR;

  constructor(public payload: any) {}
}

export type AccountActions =
  | LogoutAction
  | LogoutSuccessAction
  | SetUrlAction
  | HandleAPIErrorAction
  | APIFailureNetworkDownAction
  | UserMustConfirmEmailAction
  | CriticalAPIErrorAction;
