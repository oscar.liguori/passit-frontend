import { take, mergeMap, map, exhaustMap, catchError } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
  decryptPrivateKey,
  fromBase64,
  authenticatedEncryption,
  makeSalt,
  toBase64,
  makeRandomTypableString
} from "../../../simple_asym/crypto";
import {
  IResetPasswordLoginResponse,
  IGroup
} from "../../../passit_sdk/api.interfaces";
import { KeyPair, Password } from "../../../simple_asym/asymmetric_encryption";
import { hashPassword } from "../../../passit_sdk/hash";
import { from, throwError, forkJoin } from "rxjs";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { SetUrlAction } from "../account.actions";
import { IAuthStore, IResetPasswordVerifyResponse } from "./user.interfaces";
import { checkRespForErrors } from "../../../app/shared/utils";
import { IState, selectAuthState } from "../../app.reducers";
import { IGeneratedMFA } from "../manage-mfa/manage-mfa.interfaces";
import { environment } from "../../../environments/environment";
import { ISecret } from "../../../passit_sdk/api.interfaces";
import { decode, encode } from "cborg";
import { IU2fKey } from "../manage-u2f/manage-u2f.interfaces";

@Injectable()
export class UserService {
  constructor(
    private sdk: NgPassitSDK,
    private store: Store<IState>,
    private http: HttpClient
  ) { }

  /*
   * Check if username is available
   * returns true or false
   * if true: RegisterComponent calls register method below
   * if false: alert message triggered in RegisterComponent
   */
  public checkUsername(email: string) {
    return from(this.sdk.is_username_available(email)).pipe(
      map(isAvailable => {
        return { isAvailable };
      }),
      catchError(err => {
        let errorMessage = "Unknown Error.";
        if (err.res) {
          const errors = checkRespForErrors(err.res);
          if (errors) {
            errorMessage = errors[0];
          }
        }
        return throwError(errorMessage);
      })
    );
  }

  /* Figure out the URL before doing anything else */
  public checkAndSetUrl(url: string) {
    const apiUrl = this.normalizeServerUrl(url);
    return this.http.get(apiUrl + "ping/").pipe(
      mergeMap(() => {
        this.setSdkUrl(apiUrl);
        return apiUrl;
      }),
      catchError(err => {
        // Try api.domain (legacy recommendation)
        // Force https only
        url = "https://api." + url;
        return this.http.get(url + "ping/").pipe(
          mergeMap(() => {
            this.setSdkUrl(url);
            return url;
          }),
          catchError(() => {
            let errorMessage = "Unknown Error.";
            if (err.status || err.status === 0) {
              const errors = checkRespForErrors(err);
              if (errors) {
                errorMessage = errors[0];
              }
            }
            return throwError(errorMessage);
          })
        );
      })
    );
  }

  /** Check url to see if there is a passit server there and return resulting observable */
  checkUrl(url: string) {
    return this.http.get(this.normalizeServerUrl(url) + "ping/");
  }

  /**
   * Normalize the server url
   * - Force https in production like environments
   * - Allow some special development non https urls including localhost and 192.*
   * - Ensure url ends with /api/ (whether or not user enters it)
   * @param url url as the user entered it
   */
  private normalizeServerUrl(url: string) {
    // strip https (we'll force https later)
    url = url.replace(/^https?\:\/\//i, "");

    // normalize ending with /api/
    url = url.replace(/\/api\/?/i, "");
    url = url + "/api/";

    let apiUrl = "https://" + url;

    // for dev
    if (url === "api:8000/api/" || url === "localhost:8000/api/") {
      apiUrl = "http://" + url;
    }
    // Allow non https LAN ip address in dev
    if (environment.production === false && url.startsWith("192")) {
      apiUrl = "http://" + url;
    }
    return apiUrl;
  }

  /*
   * send login get to sdk
   * email and password to login user
   */
  public login(email: string, password: string, rememberMe: boolean) {
    // Short lived session (3 hours) when not using remember me
    const expires = rememberMe ? undefined : 3;
    return from(this.sdk.log_in(email, password, expires)).pipe(
      map(resp => {
        const auth: IAuthStore = {
          privateKey: resp.privateKey,
          publicKey: resp.user.public_key,
          userId: resp.user.id,
          email,
          userToken: resp.token,
          rememberMe,
          optInErrorReporting: resp.user.opt_in_error_reporting,
          mfaRequired: resp.user.mfa_required,
          validAuth: resp.user.valid_auth
        };
        this.setUp(auth);
        return auth;
      }),
      catchError(err => {
        let errorMessage = "Unknown Error.";
        if (err.res) {
          if (err.res.status === 401 || err.res.status === 404) {
            errorMessage = "Incorrect username or password.";
            if (err.res.status === 401 && err.res.error) {
              const LDAPError = this.checkLDAPErrors(err.res.error.detail);
              if (LDAPError) {
                errorMessage = LDAPError;
              }
            }
          } else {
            const errors = checkRespForErrors(err.res);
            if (errors) {
              errorMessage = errors[0];
            }
          }
        }
        return throwError(errorMessage);
      })
    );
  }

  public logout() {
    return this.sdk.logout();
  }

  /** Change the users password - note this can take some time.
   */
  public changePassword(oldPassword: string, newPassword: string) {
    return from(this.sdk.change_password(oldPassword, newPassword));
  }

  /** Delete User Account forever. */
  public deleteUserAccount(password: string) {
    return from(this.sdk.deleteOwnAccount(password)).pipe(
      catchError(err => {
        let errorMessage = "Unknown Error.";
        if (err.res) {
          if (err.res.status === 400) {
            errorMessage = "Incorrect password.";
          } else {
            const errors = checkRespForErrors(err.res);
            if (errors) {
              errorMessage = errors[0];
            }
          }
        }
        return throwError(errorMessage);
      })
    );
  }

  /*
   * create user post to sdk
   * pass email and password to sdk to register user
   */
  public register(email: string, password: string, rememberMe: boolean) {
    return from(this.sdk.sign_up(email, password)).pipe(
      map(resp => {
        const auth: IAuthStore = {
          privateKey: resp.privateKey,
          publicKey: resp.user.public_key,
          userId: resp.user.id,
          email,
          userToken: resp.token,
          rememberMe,
          optInErrorReporting: resp.user.opt_in_error_reporting,
          mfaRequired: resp.user.mfa_required,
          validAuth: resp.user.valid_auth
        };

        this.setUp(auth);
        return {
          ...auth,
          backupCode: resp.backupCode
        };
      }),
      catchError(err => {
        let errorMessage = "Unknown Error.";
        if (err.res) {
          const res = err.res;
          if (res.status === 400 && res.error.email) {
            const LDAPError = this.checkLDAPErrors(res.error.email[0]);
            if (LDAPError) {
              errorMessage = LDAPError;
            } else {
              errorMessage = res.error.email[0];
            }
          } else if (res.status === 401 || res.status === 404) {
            const LDAPError = this.checkLDAPErrors(err.res.error.detail);
            if (LDAPError) {
              errorMessage = LDAPError;
            }
          } else if (res.status === 400 && res.error) {
            errorMessage = res.error[0];
          } else if (res.status || res.status === 0) {
            const errors = checkRespForErrors(err);
            if (errors) {
              errorMessage = errors[0];
            }
          }
        }
        return throwError(errorMessage);
      })
    );
  }

  public resetPassword(email: string) {
    const url = this.sdk.formUrl("reset-password/");
    const data = {
      email
    };
    return this.http.post(url, data);
  }

  /** call set_keys user service method give it public and private key */
  public rehydrate() {
    return this.store
      .pipe(select(selectAuthState))
      .pipe(take(1))
      .subscribe(auth => {
        if (
          auth.publicKey &&
          auth.privateKey &&
          auth.userId &&
          auth.email &&
          auth.userToken
        ) {
          const authStore: IAuthStore = {
            email: auth.email,
            userId: auth.userId,
            privateKey: auth.privateKey,
            publicKey: auth.publicKey,
            userToken: auth.userToken,
            rememberMe: auth.rememberMe,
            optInErrorReporting: auth.optInErrorReporting,
            mfaRequired: auth.mfaRequired,
            validAuth: auth.validAuth
          };
          this.setUp(authStore);
        }
      });
  }

  public resetRegisterCode() {
    return this.sdk.request_new_confirmation().then(resp => {
      return resp;
    });
  }

  /** Part of the "forgot your password" recovery process.
   * This proves the client has the email verification code and requests
   * the private key ciphertext.
   */
  resetPasswordVerify(email: string, emailCode: string) {
    const url = this.sdk.formUrl("reset-password-verify/");
    const data = {
      email,
      code: emailCode
    };
    return this.http.post<IResetPasswordVerifyResponse>(url, data);
  }

  /** Part of the "forgot your password" recovery process.
   * This part encrypts a random message to send to the server to prove
   * the client has the private key.
   * This function sets up the sdk the same way login does and returns
   * the auth response.
   */
  resetPasswordVerifyLogin(
    email: string,
    emailCode: string,
    privateKeyBackup: string,
    backupCode: string,
    serverPublicKey: string
  ) {
    const privateKey = decryptPrivateKey(
      fromBase64(privateKeyBackup),
      backupCode
    );
    const message = makeSalt(); // Not actually a salt...just random bytes
    const encryptedMessage = authenticatedEncryption(
      fromBase64(serverPublicKey),
      privateKey,
      message
    );

    const url = this.sdk.formUrl("reset-password-login/");
    const data = {
      email: email,
      code: emailCode,
      message: toBase64(encryptedMessage)
    };
    return this.http.post<IResetPasswordLoginResponse>(url, data).pipe(
      map(resp => {
        this.sdk.set_up(
          resp.user.public_key,
          toBase64(privateKey),
          resp.user.id,
          resp.token
        );
        const auth: IAuthStore = {
          privateKey: resp.user.private_key,
          publicKey: resp.user.public_key,
          userId: resp.user.id,
          email,
          userToken: resp.token,
          rememberMe: false,
          optInErrorReporting: resp.user.opt_in_error_reporting,
          mfaRequired: resp.user.mfa_required,
          validAuth: resp.user.valid_auth
        };
        return {
          auth,
          secrets: resp.secrets,
          groups: resp.groups
        };
      })
    );
  }

  /** Final step of reset password process, set new password.
   * Unlike change password, this doesn't require the old password.
   */
  resetPasswordSetPassword(
    emailCode: string,
    newPassword: string,
    backupCode: string,
    secrets: ISecret[],
    groups: IGroup[]
  ) {
    const newUserKeyPair = KeyPair.generate();

    const { hash, salt } = hashPassword(newPassword);

    return forkJoin([
      from(this.sdk.reencryptSecretThroughSet(newUserKeyPair, secrets)),
      from(this.sdk.reencryptGroupUserSet(newUserKeyPair, groups))
    ]).pipe(
      mergeMap(([secretThroughSet, groupUserSet]) => {
        const url = this.sdk.formUrl("change-password-backup/");
        const data = {
          secret_through_set: secretThroughSet,
          group_user_set: groupUserSet,
          code: emailCode,
          user: {
            client_salt: salt,
            password: hash,
            private_key: new Password(newPassword).encrypt(
              newUserKeyPair.privateKey
            ).string,
            private_key_backup: new Password(backupCode).encrypt(
              newUserKeyPair.privateKey
            ).string,
            public_key: newUserKeyPair.publicKey.string
          }
        };
        return this.sdk.post(url, data);
      })
    );
  }

  setErrorReporting(optIn: boolean) {
    const userPatch = {
      opt_in_error_reporting: optIn
    };
    return this.sdk.update_user(this.sdk.userId, userPatch);
  }

  /**
   * Generate a new reset password backup code.
   * Send it to the server (with current password as required)
   * Return it.
   * @param currentPassword - current as typed user master password
   * @returns generated backup code
   */
  makeAndSetBackupCode(currentPassword: string) {
    const backupCode = makeRandomTypableString();
    const privateKeyBackup = new Password(backupCode).encrypt(
      this.sdk.keyPair.privateKey
    ).string;
    return from(this.sdk.api.getUser()).pipe(
      exhaustMap(user => {
        const passwordHash = hashPassword(currentPassword, user.client_salt)
          .hash;
        const url = this.sdk.formUrl(`users/${user.id}/`);
        const data = {
          current_password: passwordHash,
          private_key_backup: privateKeyBackup
        };
        return this.http.patch(url, data).pipe(
          map(resp => {
            return backupCode;
          })
        );
      })
    );
  }

  /** Generate new MFA code, returns the uri that can be imported into MFA apps like Google Authenticator */
  generateMfa() {
    const url = this.sdk.formUrl("generate-mfa/");
    return this.http.post<IGeneratedMFA>(url, "");
  }

  generateU2f(name: string) {
    const url = this.sdk.formUrl("user_keys/fido2/");
    return this.http.get(url, {
      headers: {
        Accept: "application/octet-stream"
      },
      responseType: "arraybuffer"
    })
      .pipe(map(response => {
        const converted = new Uint8Array(response);
        return decode(converted);
      }), exhaustMap(async options => {
        const credResult = await navigator.credentials.create(options);
        if (credResult == null) {
          throw Error;
        } else {
          return <PublicKeyCredential>credResult;
        }
      }), map(att => {
        if (att === undefined) {
          throw Error;
        } else {
          const attestationResponse = <AuthenticatorAttestationResponse>att.response;
          return encode({
            attestationObject: new Uint8Array(attestationResponse.attestationObject),
            clientDataJSON: new Uint8Array(attestationResponse.clientDataJSON),
            name
          });
        }
      }), exhaustMap(data => {
        if (data === undefined) {
          throw Error;
        }
        return this.http.post(url, data?.buffer, {
          headers: {
            "Content-Type": "application/cbor",
          }
        });
      }), catchError(err => {
        return throwError("Device registration was unsuccessful.");
      })
      );
  }

  getKeys() {
    const url = this.sdk.formUrl("user_keys/");
    return this.http.get<IU2fKey[]>(url);
  }

  deleteU2fKey(id: number) {
    const url = this.sdk.formUrl(`user_keys/${id}/`);
    return this.http.delete(url);
  }

  verifyU2f() {
    const url = this.sdk.formUrl("verify-fido2/");
    return this.http.get(url, {
      headers: {
        Accept: "application/octet-stream"
      },
      responseType: "arraybuffer"
    }).pipe(
      map(response => {
        const converted = new Uint8Array(response);
        return decode(converted);
      }), exhaustMap(async options => {
        const credResult = await navigator.credentials.get(options);
        if (credResult == null) {
          throw Error;
        } else {
          return credResult as PublicKeyCredential;
        }
      }), map(resp => {
        if (resp === undefined) {
          throw Error;
        } else {
          const assertionResponse = resp.response as AuthenticatorAssertionResponse;
          return encode({
            credentialId: new Uint8Array(resp.rawId),
            authenticatorData: new Uint8Array(
              assertionResponse.authenticatorData
            ),
            clientDataJSON: new Uint8Array(assertionResponse.clientDataJSON),
            signature: new Uint8Array(assertionResponse.signature),
          });
        }
      }), exhaustMap(body => {
        if (body === undefined) {
          throw Error;
        } else {
          return this.http.post(url, body.buffer, {
            headers: {
              "content-type": "application/cbor",
            }
          });
        }
      }), catchError(err => {
        return throwError("Authentication was unsuccesful.");
      })
    );
  }

  /** Activate a generated MFA code */
  activateMfa(otp: string, id: number) {
    const url = this.sdk.formUrl("activate-mfa/");
    const data = { otp, id };
    return this.http.post(url, data);
  }

  /** Verify a OTP, used for login */
  verifyMfa(otp: string) {
    const url = this.sdk.formUrl("verify-mfa/");
    const data = { otp };
    return this.http.post<null>(url, data);
  }

  /** Deactivate MFA, user will no longer need to enter MFA code to log in */
  deactivateMfa() {
    const url = this.sdk.formUrl("deactivate-mfa/");
    return this.http.post<null>(url, null);
  }

  private setSdkUrl(url: string) {
    this.store.dispatch(new SetUrlAction(url));
  }

  /** Set key and user id state for sdk object
   * Note that the auth key will be provided to the api via subscription
   * instead of using the sdk's setUp function. See api.ts
   */
  private setUp(auth: IAuthStore): Promise<void> {
    return new Promise((resolve, reject) => {
      this.sdk.set_keys(auth.publicKey, auth.privateKey);
      this.sdk.userId = auth.userId;
      this.sdk.ready().then(() => {
        resolve();
      });
    });
  }

  /** Convert server errors into nicer messages */
  private checkLDAPErrors(reason: string) {
    if (reason === "User not found in LDAP server") {
      return "User does not exist in LDAP directory. Your account may have been revoked. Contact your system administrator.";
    } else if (reason === "Unable to contact LDAP server") {
      return "Unable to contact LDAP server. Contact your system administrator.";
    } else if (reason === "Unable to authenticate to LDAP server") {
      return "Found LDAP server but was unable to authenticate using binding user. Contact your system administrator.";
    } else if (reason === "Unable to search LDAP server") {
      return "Found LDAP server but there was an error trying to search it. Contact your system administrator.";
    }
  }
}
