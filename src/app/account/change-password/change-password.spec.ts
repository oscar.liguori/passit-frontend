import { HttpClientModule } from "@angular/common/http";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { InlineSVGModule } from "ng-inline-svg";
import { NgrxFormsModule } from "ngrx-forms";
import { StoreModule } from "@ngrx/store";
import { RouterModule } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";

import { ProgressIndicatorModule } from "../../progress-indicator/progress-indicator.module";
import { ChangePasswordComponent } from "./change-password.component";
import * as fromAccount from "../account.reducer";
import * as fromRoot from "../../app.reducers";
import { ChangePasswordContainer } from ".";
import { UserService } from "../user";
import { SharedModule } from "../../shared/shared.module";
import { PasswordInputComponent } from "./password-input/password-input.component";
import { DownloadBackupCodeComponent } from "../manage-backup-code/download-backup-code/download-backup-code.component";

describe("Change Password Component", () => {
  let component: ChangePasswordContainer;
  let fixture: ComponentFixture<ChangePasswordContainer>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NgrxFormsModule,
        InlineSVGModule.forRoot(),
        RouterModule,
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        ProgressIndicatorModule,
        NoopAnimationsModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", fromAccount.reducers)
      ],
      declarations: [
        ChangePasswordContainer,
        ChangePasswordComponent,
        PasswordInputComponent,
        DownloadBackupCodeComponent
      ],
      providers: [
        {
          provide: UserService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordContainer);
    component = fixture.componentInstance;
  });

  it("should exist", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it("should display validation errors if the form is submitted empty", () => {
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#changePass").click();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Confirm your password by entering it again here, or click the eye icon to visually inspect your new password."
      ) !== -1
    ).toBe(true);
  });
});
