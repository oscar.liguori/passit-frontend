import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { Routes } from "@angular/router";
import { routes } from "./account.common";

import { LoggedInGuard } from "../guards";

import { AccountComponent } from "./account.component";
import { ManageBackupCodeContainer } from "./manage-backup-code/manage-backup-code.container";
import { DeleteContainer } from "./delete/delete.container";
import { ResetPasswordVerifyContainer } from "./reset-password/reset-password-verify/reset-password-verify.container";
import { SetPasswordContainer } from "./reset-password/set-password/set-password.container";
import { ManageMfaContainer } from "./manage-mfa/manage-mfa.container";

export const appRoutes: Routes = [
  ...routes,
  {
    path: "",
    component: AccountComponent,
    data: {
      title: "Account Management"
    }
  },
  {
    path: "change-backup-code",
    component: ManageBackupCodeContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Change Backup Code"
    }
  },
  {
    path: "delete",
    component: DeleteContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Delete Account"
    }
  },
  {
    path: "reset-password-verify",
    component: ResetPasswordVerifyContainer,
    data: {
      title: "Reset Password",
      showNavBar: false
    }
  },
  {
    path: "reset-password/set-password",
    component: SetPasswordContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Set Account Password",
      showNavBar: false
    }
  },
  {
    path: "manage-mfa",
    component: ManageMfaContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Manage MFA",
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}
