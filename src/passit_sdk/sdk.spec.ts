/* tslint:disable:max-line-length */
import fetchMock from "fetch-mock";

import { ICreateGroupResp } from "./api.interfaces";
import { AuthenticationRequiredException } from "./exceptions";
import PassitSDK from "./sdk";
import {
  clientEmail,
  createSecretResp,
  getSecretResp,
  groupDetailResp,
  groupGetResp,
  groupResp,
  groupsPostResp,
  listSecretResp,
  loginResp,
  publicUser,
  secretThroughObj,
  signUpResp,
  updateSecretResp,
  userAddGroupResp,
  userPublicKeyResp,
} from "./testResponses";

const email = clientEmail;
const testPassword = "aaaaaaaa";

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

const forceLogin = (
  sdk,
  realLogin = false,
  password = testPassword
): Promise<void> => {
  return new Promise<void>((resolve, reject) => {
    if (realLogin === false) {
      fetchMock
        .mock(`end:auth/login/`, loginResp)
        .mock(`end:user-public-auth/${email}/`, publicUser);
    }

    sdk.log_in(email, password).then((resp) => {
      resolve();
    });
  });
};

describe("PassitSDK class can", () => {
  let sdk: PassitSDK;

  beforeEach(() => {
    sdk = new PassitSDK();
  });
  afterEach(() => {
    fetchMock.restore();
  });

  it("check username availability", (done) => {
    fetchMock.mock(`end:username-available/${email}/`, { available: true });

    sdk.is_username_available(email).then((isAvailable) => {
      expect(isAvailable).toBe(true);
      done();
    });

    const email2 = "test2@test.com";
    fetchMock.mock(`end:username-available/${email2}/`, { available: false });

    sdk.is_username_available(email2).then((isAvailable) => {
      expect(isAvailable).toBe(false);
      done();
    });
  });

  it("sign up", (done) => {
    const password = testPassword;

    fetchMock.mock(`end:users/`, signUpResp).mock(`end:auth/login/`, loginResp);

    sdk.sign_up(email, password).then((resp) => {
      expect(resp.user.email).toBe(email);
      done();
    });
  });

  it("log in", (done) => {
    const password = testPassword;

    fetchMock
      .mock(`end:user-public-auth/${email}/`, publicUser)
      .mock(`end:auth/login/`, loginResp);

    sdk.log_in(email, password).then((resp) => {
      expect(resp).toBeTruthy();
      done();
    });
  });

  it("log in but server is down", (done) => {
    const password = testPassword;

    fetchMock
      .mock(`end:user-public-auth/${email}/`, {})
      .mock(`end:auth/login/`, { throws: "error" });

    sdk.log_in(email, password).catch((err) => {
      expect(err).toBeTruthy();
      done();
    });
  });

  it("deny incorrect log in", (done) => {
    const password = "1234567";

    fetchMock
      .mock(`end:user-public-auth/${email}/`, publicUser)
      .mock(`end:auth/login/`, {
        status: 401,
        body: { detail: "Invalid username/password." },
      });

    sdk
      .log_in(email, password)
      .then((resp) => {
        fail("Should not have been able to log in");
      })
      .catch((ex) => {
        expect(ex).not.toBe(null);
        done();
      });
  });

  it("create group", (done) => {
    fetchMock.post(`end:groups/`, {
      status: 201,
      body: { id: 1, name: "mygroup", slug: "mygroup" },
    });

    forceLogin(sdk).then(() => {
      const group = "mygroup";
      sdk.create_group(group, group).then((resp) => {
        expect(resp.name).toBe(group);
        done();
      });
    });
  });

  it("create group and generate slug", (done) => {
    fetchMock.post(`end:groups/`, {
      status: 201,
      body: { id: 1, name: "My Group", slug: "my-group" },
    });

    forceLogin(sdk).then(() => {
      const group = "My Group";
      sdk.create_group(group).then((resp) => {
        expect(resp.name).toBe(group);
        done();
      });
    });
  });

  it("update existing group", (done) => {
    const groupResp1 = '{"id":38,"name":"My Group","slug":"my-group"}';
    const groupResp2 = '{"id":38,"name":"My Edited Group","slug":"my-group"}';
    fetchMock
      .post(`end:groups/`, { status: 201, body: groupResp1 })
      .patch(`end:groups/38/`, groupResp2);

    const group = "My Group";
    forceLogin(sdk).then(() => {
      sdk.create_group(group).then((newGroup) => {
        const newName = "My Edited Group";
        newGroup.name = newName;
        sdk.update_group(newGroup).then((updatedGroup) => {
          expect(updatedGroup.name).toBe(newName);
          done();
        });
      });
    });
  });

  it("fail to create group and when not logged in", () => {
    const group = "My Group";
    sdk.create_group(group).catch((err) => {
      expect(err).toEqual(new AuthenticationRequiredException("create_group"));
    });
  });

  it("add user to group", (done) => {
    const groupId = 1;
    const userId = 2;
    fetchMock
      .get(`end:groups/${groupId}/`, groupDetailResp)
      .get(`end:user-public-key/${userId}/`, userPublicKeyResp)
      .post(`end:groups/${groupId}/users/`, userAddGroupResp);

    forceLogin(sdk).then(() => {
      sdk
        .add_user_to_group(groupId, userId, "test@example.com")
        .then((resp) => {
          expect(resp.user).toBe(userId);
          expect(resp.group).toBe(groupId);
          done();
        });
    });
  });

  it("can list contacts", (done) => {
    const contact = {
      id: 5,
      email: "test@example.com",
      first_name: "My",
      last_name: "Friend",
    };

    fetchMock.get(`end:contacts/`, [contact]);

    forceLogin(sdk).then(() => {
      sdk.list_contacts().then((contacts) => {
        expect(contacts[0].id).toBe(5);
        done();
      });
    });
  });

  it("remove user from group", (done) => {
    const groupId = 1;
    const groupUserId = 1;
    fetchMock.delete(`end:groups/${groupId}/users/${groupUserId}/`, {
      status: 204,
    });

    forceLogin(sdk).then(() => {
      sdk.remove_user_from_group(groupId, groupUserId).then(() => {
        done();
      });
    });
  });

  it("delete groups", (done) => {
    fetchMock
      .post("end:groups/", { id: 52, name: "Test Group", slug: "test-group" })
      .delete(`end:groups/52/`, { status: 204 });

    forceLogin(sdk).then(() => {
      sdk.create_group("Test Group").then((group) => {
        sdk.delete_group(group.id).then(() => {
          done();
        });
      });
    });
  });

  it("get group", (done) => {
    const groupId = 7;
    fetchMock.get(`end:groups/${groupId}/`, groupResp);

    forceLogin(sdk).then(() => {
      sdk.get_group(groupId).then((resp) => {
        expect(resp.name).toBe("devs");
        done();
      });
    });
  });

  it("list groups", (done) => {
    fetchMock.get(`end:groups/`, [groupResp]);

    forceLogin(sdk).then(() => {
      sdk.list_groups().then((resp) => {
        expect(resp[0].name).toBe("devs");
        done();
      });
    });
  });

  it("create secret", (done) => {
    const name = "my secret";
    const type = "website";
    const visibleData = {
      username: "myself",
    };
    const secrets = {
      password: "hunter2",
    };

    fetchMock.post(`end:secrets/`, createSecretResp);

    forceLogin(sdk).then(() => {
      sdk
        .create_secret({
          name,
          type,
          visible_data: visibleData,
          secrets,
        })
        .then((resp) => {
          expect(resp.name).toBe(name);
          expect(resp.type).toBe(type);
          done();
        });
    });
  });

  it("create secret handles errors", (done) => {
    const name = "";
    const type = "website";
    const visibleData = {
      username: "",
    };

    forceLogin(sdk).then(() => {
      sdk
        .create_secret({
          name,
          type,
          visible_data: visibleData,
        })
        .catch((err) => {
          expect(err).toBeTruthy();
          done();
        });
    });
  });

  it("raises nice errors on unauthed create secret", async () => {
    const name = "my secret";
    const type = "website";
    const visibleData = {
      username: "myself",
    };
    const secrets = {
      password: "hunter2",
    };
    const data = {
      name,
      type,
      visible_data: visibleData,
      secrets,
    };
    try {
      await sdk.create_secret(data);
    } catch (err) {
      expect(err).toEqual(new AuthenticationRequiredException("create_secret"));
    }
  });

  it("create group secret", (done) => {
    const name = "my group secret";
    const type = "website";
    const visibleData = {
      username: "myself",
    };
    const secrets = {
      password: "hunter2",
    };

    const groupId = 7;
    const groupName = "My Group";

    const publicKey = "xRhhY/M0r65jDMRjoMVcgHEdZyj1EXGDyLGXzPYByQI=";
    const myKeyCiphertext =
      "Ib1dn1oOWxCHsUqvCfpOzMThY1fmFVYyy9O+4cokXBXMsLbkPTu7inPvf66iB/TEqld31O7ubOBTUoCvfKUpNCIOVxzUOGwC964DcNsdeL61IvkhOSrvmroP/Yk=";
    const myPrivateKeyCiphertext =
      "zSxd4cJX5MyyHvhU/nJvvEzb9fu1WASm9t4UybiLrtyzmpFzfUPYtPxEd7rZkPnDu/NI5UB8UwSrhfLc+huCfOvYfDII2lCbKfPNYsUruztrYgTk";
    const password =
      "ypXHWRqYEuWb7eNvdTOtHRCjzqxaVH3/QJuKWRy+DyPKRFGKn1LYQzGxr1GnTOo=";
    const keyCiphertext =
      "T1vB9Omm7lLs1w4IcxgxHG1G6lZxeu61ob5uiLn6rWXET7bIiUgvRMWqvZua/JR/DxJWEfxcKJIcpBRsSIYMI7Ga4QT7kI+96cpdNEGdUJ3F7UhkH/EqbbgZeSM=";
    const groupPostResp = { id: 7, name: "My Group", slug: "my-group" };
    const groupGetResp1 = {
      id: 7,
      name: "My Group",
      groupuser_set: [{ id: 7, user: 1, group: 7, is_group_admin: true }],
      public_key: publicKey,
      my_key_ciphertext: myKeyCiphertext,
      my_private_key_ciphertext: myPrivateKeyCiphertext,
    };
    const secretPostResp = {
      id: 8,
      name: "my group secret",
      type: "website",
      data: { username: "myself" },
      secret_through_set: [
        {
          id: 8,
          group: 7,
          key_ciphertext: keyCiphertext,
          data: { password },
          public_key: publicKey,
          is_mine: true,
        },
      ],
    };

    fetchMock
      .post(`end:groups/`, groupPostResp)
      .get(`end:groups/${groupId}/`, groupGetResp1)
      .post(`end:secrets/`, secretPostResp);

    forceLogin(sdk).then(() => {
      sdk.create_group(groupName).then((group) => {
        sdk
          .create_secret({
            name,
            type,
            group_id: group.id,
            visible_data: visibleData,
            secrets,
          })
          .then((resp) => {
            expect(resp.name).toBe(name);
            expect(resp.type).toBe(type);
            sdk.decrypt_secret(resp).then((decrypted) => {
              expect(decrypted).toEqual(secrets);
              done();
            });
          });
      });
    });
  });

  it("can decrypt a secret without making api calls", async () => {
    const name = "my secret";
    const type = "website";
    const visibleData = {
      username: "myself",
    };
    const secrets = {
      password: "hunter4",
    };

    fetchMock.post(`end:secrets/`, createSecretResp);

    await forceLogin(sdk);
    const resp = await sdk.create_secret({
      name,
      type,
      visible_data: visibleData,
      secrets,
    });

    const { key_ciphertext, data } = resp.secret_through_set.find(
      (through) => through.is_mine === true
    );
    const result = sdk.offline_decrypt_secret(key_ciphertext, data);
    expect(result.password).toBe("hunter4");
  });

  it("update secret partially with patch", (done) => {
    const secretId = 1;
    const newName = "blarg";

    fetchMock.mock(`end:secrets/${secretId}/`, updateSecretResp, {
      method: "PATCH",
    });

    forceLogin(sdk).then(() => {
      sdk
        .update_secret({
          id: secretId,
          name: newName,
        })
        .then((resp) => {
          expect(resp.name).toBe(newName);
          done();
        });
    });
  });

  it("update secret full with put", (done) => {
    const secretId = 1;
    const newSecret = {
      password: "lol",
    };
    const putSecretResp = Object.assign({}, updateSecretResp);
    const publicKey = "xRhhY/M0r65jDMRjoMVcgHEdZyj1EXGDyLGXzPYByQI=";
    const keyCiphertext =
      "T1vB9Omm7lLs1w4IcxgxHG1G6lZxeu61ob5uiLn6rWXET7bIiUgvRMWqvZua/JR/DxJWEfxcKJIcpBRsSIYMI7Ga4QT7kI+96cpdNEGdUJ3F7UhkH/EqbbgZeSM=";
    const password =
      "ypXHWRqYEuWb7eNvdTOtHRCjzqxaVH3/QJuKWRy+DyPKRFGKn1LYQzGxr1GnTOo=";
    putSecretResp.secret_through_set = [
      {
        id: 7,
        group: null,
        key_ciphertext: keyCiphertext,
        data: { password },
        public_key: publicKey,
        is_min: true,
      },
    ];

    fetchMock
      .get(`end:secrets/${secretId}/`, getSecretResp)
      .put(`end:secrets/${secretId}/`, putSecretResp);

    forceLogin(sdk).then(() => {
      sdk
        .update_secret({
          id: secretId,
          secrets: newSecret,
        })
        .then((resp) => {
          expect(resp.secret_through_set).toEqual(
            putSecretResp.secret_through_set
          );
          done();
        });
    });
  });

  it("Create group and decrypt it's key", (done) => {
    const groupName = "My Group";

    fetchMock
      .post(`end:groups/`, groupsPostResp)
      .get(`end:groups/51/`, groupGetResp);

    forceLogin(sdk).then(() => {
      sdk.create_group(groupName).then((createdGroup) => {
        sdk.get_group(createdGroup.id).then((group) => {
          const keyPair = (sdk as any)._getGroupKeyPair(group);
          expect(keyPair.privateKey).toBeTruthy();
          done();
        });
      });
    });
  });

  /** Integration test requires real backend, don't run in CI
   */
  xit("reset password with real backend", (done) => {
    const name = "my secret";
    const oldPassword = testPassword;
    const type = "website";
    const secrets = {
      password: "hunter2",
    };
    const newSecret = {
      name,
      type,
      visible_data: {},
      secrets,
    };
    const groupName = "My Group";
    const otherUserId = 2;
    const otherUserEmail = "noadmin@aa.aa";
    const newMasterPassword = "my great pass";

    const makeSharedSecret = (): Promise<any> => {
      return new Promise((resolve, reject) => {
        sdk.create_group(groupName).then((group) => {
          sdk.create_secret(newSecret).then((secret) => {
            sdk.add_group_to_secret(group.id, secret).then(() => {
              sdk
                .add_user_to_group(group.id, otherUserId, otherUserEmail)
                .then(() => {
                  sdk.get_secret(secret.id).then((secret2) => {
                    sdk.decrypt_secret(secret).then((resp) => {
                      expect(resp.password).toBe("hunter2");
                      resolve(secret2);
                    });
                  });
                });
            });
          });
        });
      });
    };

    forceLogin(sdk, true).then(() => {
      sdk.create_secret(newSecret).then((secret2) => {
        makeSharedSecret().then((secret) => {
          setTimeout(() => {
            sdk.change_password(oldPassword, newMasterPassword).then(() => {
              sdk = new PassitSDK();
              forceLogin(sdk, true, newMasterPassword).then(() => {
                sdk.get_secret(secret2.id).then((s2) => {
                  sdk.decrypt_secret(s2).then((r2) => {
                    expect(r2.password).toBe("hunter2");
                  });
                });
                sdk.get_secret(secret.id).then((secret3) => {
                  sdk.decrypt_secret(secret3).then((resp2) => {
                    expect(resp2.password).toBe("hunter2");
                    done();
                  });
                });
              });
            });
          }, 100);
        });
      });
    });
  }, 20000);

  /** Integration test requires real backend, don't run in CI
   * Create a secret and group
   * Then add the group to the secret
   * Then update the secret
   * After each step - check that the descrypted secret is as expected
   * Sorry for the async hell! It's just an integration test.
   */
  /* tslint:disable:no-shadowed-variable */
  xit("update secret and decrypt it with real backend", (done) => {
    const name = "my secret";
    const type = "website";
    const visibleData = {
      username: "myself",
    };
    const secrets = {
      password: "hunter2",
    };
    const newSecret = {
      name,
      type,
      visible_data: visibleData,
      secrets,
    };
    const groupName = "My Group";
    let group: ICreateGroupResp;
    const otherUserId = 2;
    const otherUserEmail = "noadmin@aa.aa";

    forceLogin(sdk, true).then(() => {
      sdk.create_group(groupName).then((resp) => {
        group = resp;
      });

      sdk.create_secret(newSecret).then((secret) => {
        sdk.decrypt_secret(secret).then((resp) => {
          expect(resp.password).toBe("hunter2");
        });
        const updatedSecret = Object.assign(newSecret, {
          id: secret.id,
          secrets: { password: "hunter3" },
        });
        sdk.update_secret(updatedSecret).then((updatedSecretResp) => {
          sdk.decrypt_secret(updatedSecretResp).then((resp) => {
            expect(resp.password).toBe("hunter3");
            sdk.add_group_to_secret(group.id, secret.id).then(() => {
              sdk
                .add_user_to_group(group.id, otherUserId, otherUserEmail)
                .then(() => {
                  sdk.get_secret(secret.id).then((secret) => {
                    sdk.decrypt_secret(secret).then((resp) => {
                      expect(resp.password).toBe("hunter3");
                      const editedSecret = {
                        id: secret.id,
                        secrets: {
                          password: "hunter4",
                        },
                      };
                      sdk.update_secret(editedSecret).then((secret) => {
                        sdk.decrypt_secret(secret).then((resp) => {
                          expect(resp.password).toBe("hunter4");
                          done();
                        });
                      });
                    });
                  });
                });
            });
          });
        });
      });
    });
  });
  /* tslint:enable:no-shadowed-variable */

  it("get secret", (done) => {
    const secretId = 1;
    fetchMock.get(`end:secrets/${secretId}/`, getSecretResp);

    forceLogin(sdk).then(() => {
      sdk.get_secret(secretId).then((resp) => {
        expect(resp.name).toBe("my secret");
        done();
      });
    });
  });

  it("get and decrypt secret", (done) => {
    const secretId = 1;

    fetchMock.get(`end:secrets/${secretId}/`, getSecretResp);

    forceLogin(sdk).then(() => {
      sdk.get_secret(secretId).then((resp) => {
        sdk.decrypt_secret(resp).then((secret) => {
          expect(secret.password).toBe("hunter4");
          done();
        });
      });
    });
  });

  it("delete secret", (done) => {
    const secretId = 9;

    fetchMock.mock(`end:secrets/${secretId}/`, { status: 204 });

    forceLogin(sdk).then(() => {
      sdk.delete_secret(secretId).then(() => {
        done();
      });
    });
  });

  it("list secrets", (done) => {
    fetchMock.get(`end:secrets/`, listSecretResp);

    forceLogin(sdk).then(() => {
      sdk.list_secrets().then((resp) => {
        expect(resp[0].name).toBe("my secret");
        done();
      });
    });
  });

  it("add group to secret", (done) => {
    const secretId = 1;
    const groupId = 1;
    fetchMock
      .get(`end:groups/${groupId}/`, groupDetailResp)
      .get(`end:secrets/${secretId}/`, getSecretResp)
      .post(`end:secrets/9/groups/`, secretThroughObj);

    forceLogin(sdk).then(() => {
      sdk.add_group_to_secret(groupId, secretId).then((resp) => {
        expect(resp.is_mine).toBe(true);
        done();
      });
    });
  });

  it("remove group from secret", (done) => {
    const secretId = 1;
    const secretThroughId = 1;
    fetchMock.delete(`end:secrets/${secretId}/groups/${secretThroughId}/`, {
      status: 204,
    });

    forceLogin(sdk).then(() => {
      sdk.remove_group_from_secret(secretId, secretThroughId).then(() => {
        done();
      });
    });
  });

  it("change password", (done) => {
    const oldPassword = testPassword;
    const newPassword = "MyNewPassword";

    fetchMock
      .get(`end:secrets/`, [getSecretResp])
      .get(`end:groups/`, [groupResp])
      .get(`end:groups/1/`, groupResp)
      .get(`end:users/`, [signUpResp])
      .post(`end:change-password/`, { success: true });

    forceLogin(sdk).then(() => {
      sdk.change_password(oldPassword, newPassword).then(() => {
        done();
      });
    });
  });
});
