export const environment = {
  production: true,
  extension: true,
  docker: false,
  VERSION: require("../../package.json").version
};
